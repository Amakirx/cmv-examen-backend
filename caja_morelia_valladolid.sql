-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-08-2021 a las 18:33:33
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `caja_morelia_valladolid`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_client_accounts` (IN `id` INT)  BEGIN
	SELECT * FROM tbl_cmv_cliente c INNER JOIN tbl_cmv_cliente_cuenta cc ON c.id_cliente = cc.id_cliente INNER JOIN cat_cmv_tipo_cuenta ctc on cc.id_cuenta = ctc.id_cuenta WHERE c.id_cliente = id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_client_details` (`id` INT)  BEGIN
	SELECT * FROM tbl_cmv_cliente WHERE id_cliente = id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_client` (`id` INT)  BEGIN
	DELETE FROM tbl_cmv_cliente WHERE id_cliente = id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_edit_client` (IN `id` INT, IN `name` VARCHAR(15), IN `lastname` VARCHAR(15), IN `lastname2` VARCHAR(15), IN `new_rfc` VARCHAR(15), IN `new_curp` VARCHAR(18))  BEGIN
	UPDATE tbl_cmv_cliente SET nombre = name, apellido_paterno = lastname, apellido_materno = lastname2, rfc = new_rfc, curp = new_curp
    WHERE id_cliente = id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_client` (IN `name` VARCHAR(15), IN `lastname` VARCHAR(15), IN `lastname2` VARCHAR(15), IN `rfc` VARCHAR(13), IN `curp` VARCHAR(18))  BEGIN
	INSERT INTO tbl_cmv_cliente (nombre,apellido_paterno,apellido_materno,rfc,curp) VALUES (name,lastname,lastname2,rfc,curp);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_show_clients` ()  BEGIN
	SELECT * FROM tbl_cmv_cliente;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_cmv_tipo_cuenta`
--

CREATE TABLE `cat_cmv_tipo_cuenta` (
  `id_cuenta` int(11) NOT NULL,
  `nombre_cuenta` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cat_cmv_tipo_cuenta`
--

INSERT INTO `cat_cmv_tipo_cuenta` (`id_cuenta`, `nombre_cuenta`) VALUES
(1, 'Credito'),
(2, 'Debito');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_cmv_cliente`
--

CREATE TABLE `tbl_cmv_cliente` (
  `id_cliente` int(11) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `apellido_paterno` varchar(15) NOT NULL,
  `apellido_materno` varchar(15) NOT NULL,
  `rfc` varchar(13) NOT NULL,
  `curp` varchar(18) NOT NULL,
  `fecha_alta` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_cmv_cliente`
--

INSERT INTO `tbl_cmv_cliente` (`id_cliente`, `nombre`, `apellido_paterno`, `apellido_materno`, `rfc`, `curp`, `fecha_alta`) VALUES
(11, 'Isaac', 'Rebollar', 'Bahena', 'ISRE19099600', 'ISRE190996HHHMMM01', '2021-08-15'),
(12, 'Andres', 'Perez', 'Zavala', 'PEZA960919000', 'PEZA960919HMNRVN09', '2021-08-03'),
(13, 'Mario', 'Carbajal', 'Aburto', 'MACA950110000', 'MACAHMNRVN00', '2021-08-12'),
(14, 'Ruben', 'Herrejon', 'Valencia', 'RUHE960715123', 'RUHE960715HMNHNM01', '2021-08-03'),
(15, 'Pedro', 'Infante', 'Magdaleno', 'PEIN851015321', 'PEIN851015HNRVN02', '2021-08-12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_cmv_cliente_cuenta`
--

CREATE TABLE `tbl_cmv_cliente_cuenta` (
  `id_cliente_cuenta` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_cuenta` int(11) NOT NULL,
  `saldo_actual` float NOT NULL,
  `fecha_contratacion` date NOT NULL DEFAULT current_timestamp(),
  `fecha_ultimo_movimiento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_cmv_cliente_cuenta`
--

INSERT INTO `tbl_cmv_cliente_cuenta` (`id_cliente_cuenta`, `id_cliente`, `id_cuenta`, `saldo_actual`, `fecha_contratacion`, `fecha_ultimo_movimiento`) VALUES
(3, 11, 2, 100000, '2021-08-13', '2021-08-15'),
(4, 12, 1, 25000, '2021-08-15', '2021-08-16'),
(5, 13, 2, 37000, '2021-08-14', '2021-08-16'),
(6, 14, 2, 5000, '2021-08-16', '2021-08-16'),
(7, 15, 1, 8800.5, '2021-08-13', '2021-08-14');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cat_cmv_tipo_cuenta`
--
ALTER TABLE `cat_cmv_tipo_cuenta`
  ADD PRIMARY KEY (`id_cuenta`);

--
-- Indices de la tabla `tbl_cmv_cliente`
--
ALTER TABLE `tbl_cmv_cliente`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `tbl_cmv_cliente_cuenta`
--
ALTER TABLE `tbl_cmv_cliente_cuenta`
  ADD PRIMARY KEY (`id_cliente_cuenta`),
  ADD KEY `fk_cliente` (`id_cliente`),
  ADD KEY `fk_cuenta` (`id_cuenta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cat_cmv_tipo_cuenta`
--
ALTER TABLE `cat_cmv_tipo_cuenta`
  MODIFY `id_cuenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_cmv_cliente`
--
ALTER TABLE `tbl_cmv_cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `tbl_cmv_cliente_cuenta`
--
ALTER TABLE `tbl_cmv_cliente_cuenta`
  MODIFY `id_cliente_cuenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbl_cmv_cliente_cuenta`
--
ALTER TABLE `tbl_cmv_cliente_cuenta`
  ADD CONSTRAINT `fk_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `tbl_cmv_cliente` (`id_cliente`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_cuenta` FOREIGN KEY (`id_cuenta`) REFERENCES `cat_cmv_tipo_cuenta` (`id_cuenta`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
