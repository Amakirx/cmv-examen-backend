// importacion de librerias y dependencias
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const clientController = require("./controller/clients")

const config = require( './config/enviroment')

// Inicializar express
const app = express();

// Uso de libreria body parser para poder comunicacion en formato JSON
app.use(bodyParser.json());
app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));

// Rutas para acceso a los endpoints
app.get("/clients", clientController.getAllClients );
app.get("/clients/:clientId", clientController.getClient);
app.get("/accounts/:clientId", clientController.getClientAccounts);
app.post("/create", clientController.createClient);
app.patch("/edit/:clientId", clientController.editClient);
app.delete("/delete/:clientId", clientController.deleteClient);

// Asignacion de puerto en el que correra la API
app.listen(config.HTTP_PORT, () => {
  console.log("Server is running on port "+ config.HTTP_PORT);
});