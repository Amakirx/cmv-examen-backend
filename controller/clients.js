// importacion de librerias y dependencias
const connection = require('../config/db.js')

// Funcion para obtener todos los clientes registrados
function getAllClients(request, response) {
    // LLamadao del Stored Procedure y envio de respuesta al Frontend
    connection.query("call sp_show_clients", true, (err, result, fields) => {
        if (err) response.send(err.message)
        if (result[0].length == 0) return response.json("No hay clientes registrados")
        response.json(result[0])
    })
}

// Funcion para obtener los detalles de un cliente
function getClient(request, response) {
    // Asignacion de valor obtenido por el frontend
    const clientId = request.params.clientId
    // Validacion para control de errores
    if (isNaN(clientId)) return response.json("Ingrese un valor numerico")
    // LLamadao del Stored Procedure y envio de respuesta al Frontend
    connection.query("call sp_client_details(?)", [clientId], (err, result, fields) => {
        if (err) response.json(err.message)
        if (result[0].length == 0) return response.json("El cliente no existe")
        response.json(result[0])

    })

}
// Funcion para creacion de nuevo usuario
function createClient(request, response) {
    // Asignacion de valores obtenidos por el Frontend
    const name = request.body.name
    const lastname = request.body.lastname
    const lastname2 = request.body.lastname2
    const rfc = request.body.rfc.toUpperCase()
    const curp = request.body.curp.toUpperCase()

    // Validaciones para control de errores
    if (!name) return response.json("Ingrese un valor para el nombre")
    if (!lastname) return response.json("Ingrese un valor para el apellido materno")
    if (!lastname2) return response.json("Ingrese un valor para el apellido paterno")
    if (!rfc) return response.json("Ingrese un valor para el nombre")
    if (!curp) return response.json("Ingrese un valor para el nombre")

    // LLamadao del Stored Procedure y envio de respuesta al Frontend
    connection.query("call sp_insert_client(?,?,?,?,?)", [name, lastname, lastname2, rfc, curp], (err, result, fields) => {
        if (err) response.send(err.message)
        response.json({ succes: true })
    })
}

function editClient(request, response) {
    // Asignacion de valores obtenidos por el Frontend
    const clientId = request.params.clientId
    if (isNaN(clientId)) return response.json("Ingrese un valor numerico")
    
    const name = request.body.name
    const lastname = request.body.lastname
    const lastname2 = request.body.lastname2
    const rfc = request.body.rfc.toUpperCase()
    const curp = request.body.curp.toUpperCase()
    
    // Validaciones para control de errores
    if (!name) return response.json("Ingrese un valor para el nombre")
    if (!lastname) return response.json("Ingrese un valor para el apellido materno")
    if (!lastname2) return response.json("Ingrese un valor para el apellido paterno")
    if (!rfc) return response.json("Ingrese un valor para el nombre")
    if (!curp) return response.json("Ingrese un valor para el nombre")

    // LLamadao del Stored Procedure y envio de respuesta al Frontend
    connection.query("call sp_edit_client(?,?,?,?,?,?)", [clientId, name, lastname, lastname2, rfc, curp], (err, result, fields) => {
        if (err) response.send(err.message)
        if (result.affectedRows == 0) return response.json("El cliente no existe o no ha sido Editado")
        response.json({ succes: true })
    })

}

function getClientAccounts(request, response) {
    // Asignacion de valores obtenidos por el Frontend
    const clientId = request.params.clientId
    if (isNaN(clientId)) return response.json("Ingrese un valor numerico")
    // LLamadao del Stored Procedure y envio de respuesta al Frontend
    connection.query("call sp_client_accounts(?)", [clientId], (err, result, fields) => {
        if (err) response.json(err.message)
        if (result[0].length == 0) return response.json("El cliente no existe o no tiene cuentas asociadas")
        response.json(result[0])

    })
}

function deleteClient(request, response) {

    const clientId = request.params.clientId

    if (isNaN(clientId)) return response.json("Ingrese un valor numerico")
    // LLamadao del Stored Procedure y envio de respuesta al Frontend
    connection.query("call sp_delete_client(?)", [clientId], (err, result, fields) => {
        if (err) response.json(err.message)
        if (result.affectedRows == 0) return response.json("El cliente no existe o no ha sido eliminado")
        response.json({ succes: true })

    })
}

// exportacion de los metodos para su uso en las rutas
module.exports = {
    getAllClients,
    getClient,
    createClient,
    editClient,
    getClientAccounts,
    deleteClient
}