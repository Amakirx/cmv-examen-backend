// importacion de librerias y dependencias
const dotenv  = require("dotenv")
const path = require("path")
dotenv.config({path: path.resolve(__dirname,'../local.env')})

// Asigacion de variables en base a los valores de las variables de entorno
module.exports = {
   
    DB_HOST : process.env.DB_HOST,
    DB_PORT : process.env.DB_PORT,
    DB_USERNAME : process.env.DB_USERNAME,
    DB_PASSWORD : process.env.DB_PASSWORD,
    DB_DATABASE : process.env.DB_DATABASE,
    DB_LOGGING : process.env.DB_LOGGING,

    HTTP_PORT : process.env.HTTP_PORT
}