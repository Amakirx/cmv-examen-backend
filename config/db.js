// importacion de librerias y dependencias
const mysql = require("mysql");
const config = require( './enviroment')


// Configuracion de la conexion a la base de datos
const connection = mysql.createConnection({
  host: config.DB_HOST,
  user: config.DB_USERNAME,
  password: config.DB_PASSWORD,
  database: config.DB_DATABASE
});

//Abrir conexion con la base de datos
connection.connect(error => {
  if (error) throw error;
  console.log("Successfully connected to the database.");
});

module.exports = connection;